Gitlog 2 changelog
------------------

Scans GitLab-flavoured gitlog and looks for merged branches. Outputs a formatted changelog.

```
$ cd myproject
$ php path-to/gitlog2changelog/gitlog2changelog.php > CHANGELOG
```


Example output:

```
04. Oct 2018
------------
- #213 - Add support for messaging (Some Owner)
- #114 - Layout broken in latest Safari (Some Owner)

02. Oct 2018
------------
- #112 - Fixed some other ting (Some Owner)
```



License: MIT