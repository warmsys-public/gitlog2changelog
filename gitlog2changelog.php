<?php
ini_set('display_errors', 'Off');

class Gitlog2Changelog
{
    public function getChangelog(string $gitlog)
    {
        $changelog = [];
        $items = $this->parseLog($gitlog);

        $branchesMergedElsewhere = [];
        foreach ($items as $item)
        {
            $commitMessage = $item['message'];
            if (!$this->isMergeCommit($commitMessage))
            {
                continue;
            }

            list($targetBranch, $mergedBranch) = $this->getBranchesInMerge($commitMessage);

            if ($mergedBranch == 'master')
            {
                continue;
            }

            $formattedMergedBranch = $this->formatMergedBranch($mergedBranch);
            $author = $this->formatAuthor($item['author']);
            $formattedMergedBranch .= " ($author)";

            $date = new DateTime($item['date']);
            $formattedDate = $date->format('d. M Y');

            if ($targetBranch != 'master')
            {
                $branchesMergedElsewhere[$targetBranch] = $formattedMergedBranch;
                continue;
            }

            if (!isset($changelog[$formattedDate]) || !is_array($changelog[$formattedDate]))
            {
                $changelog[$formattedDate] = [];
            }

            $changelog[$formattedDate][] = '- ' . $formattedMergedBranch;
            if (isset($branchesMergedElsewhere[$formattedMergedBranch]))
            {
                foreach ($branchesMergedElsewhere[$formattedMergedBranch] as $elseWhereBranch)
                {
                    $changelog[$formattedDate][] = '- ' . $elseWhereBranch;
                }
                unset($branchesMergedElsewhere[$formattedMergedBranch]);
            }
        }

        $this->printChangelog($changelog);
    }

    private function parseLog(string $log): array
    {
        $lines = explode("\n", $log);
        $history = [];
        foreach ($lines as $key => $line)
        {
            if (strpos($line, 'commit') === 0 || $key + 1 == count($lines))
            {
                if (!empty($commit))
                {
                    $commit['message'] = substr($commit['message'], 4);
                    array_push($history, $commit);
                    unset($commit);
                }
                $commit['hash'] = substr($line, strlen('commit') + 1);
            }
            else if (strpos($line, 'Author') === 0)
            {
                $commit['author'] = substr($line, strlen('Author:') + 1);
            }
            else if (strpos($line, 'Date') === 0)
            {
                $commit['date'] = substr($line, strlen('Date:') + 3);
            }
            elseif (strpos($line, 'Merge') === 0)
            {
                $commit['merge'] = substr($line, strlen('Merge:') + 1);
                $commit['merge'] = explode(' ', $commit['merge']);
            }
            else
            {
                if (isset($commit['message']))
                {
                    $commit['message'] .= $line;
                }
                else
                {
                    $commit['message'] = $line;
                }
            }
        }
        return $history;
    }

    private function getBranchesInMerge(string $commitMessage): array
    {
        preg_match_all('/Merge branch \'(.+)\' into (.+)/', $commitMessage, $result);
        $targetBranch = $result[2][0];
        if (stripos($targetBranch, "'master'") === 0)
        {
            $targetBranch = 'master';
        }
        $mergedBranch = $result[1][0];


        if (!$mergedBranch && !$targetBranch)
        {

            preg_match('/Merge branch \'(.+)\'/', $commitMessage, $result);
            $targetBranch = 'master';
            $mergedBranch = $result[1];

        }
        return [$targetBranch, $mergedBranch];
    }

    private function formatAuthor(string $author): string
    {
        preg_match_all('/(.+)\</', $author, $result);
        return trim($result[1][0]);
    }

    private function isMergeCommit(string $commitMessage): bool
    {
        return stripos($commitMessage, 'Merge branch') !== false;
    }

    private function formatMergedBranch(string $mergedBranch): string
    {
        $formatted = '';
        preg_match_all('/(\d+)-(.+)/', $mergedBranch, $result);
        if (isset($result[2][0]))
        {

            $ticketId = $result[1][0];
            $branchName = $result[2][0];
            $branch = str_replace('-', ' ', $branchName);
            $branch = ucfirst($branch);
            $formatted = '#' . $ticketId . ' - ' . $branch;
        }
        else
        {
            $branch = str_replace('-', ' ', $mergedBranch);
            $branch = ucfirst($branch);
            $formatted = $branch;
        }
        return $formatted;
    }

    private function printChangelog(array $changelog)
    {
        foreach ($changelog as $date => $values)
        {
            echo "$date\n";
            echo "------------\n";
            foreach ($values as $value)
            {
                echo "$value\n";
            }
            echo "\n";
        }
    }
}

$log = shell_exec('git log --merges');

$gitlog2changelog = new Gitlog2Changelog();
$gitlog2changelog->getChangelog($log);